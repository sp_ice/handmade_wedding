var initialData = [
	{
		g_category_selected:'新婦',
		g_name:"親族",
		members: [
			{ lname: "田中",
			  fname: "太郎",
			  zip: "100-0000",
			  address: "東京都千代田区",
			  phone: "090-9999-9999",
			  st01: false,
			  st02: false,
			  st03: false,
			  st04: false,
			  st05: false
			},
			{ lname: "田中",
			  fname: "二郎",
			  zip: "100-0001",
			  address: "東京都千代田区千代田",
			  phone: "090-9999-9999",
			  st01: false,
			  st02: false,
			  st03: false,
			  st04: false,
			  st05: false
			}
		]
	},
	{
		g_category_selected:'新郎',
		g_name:"会社",
		members: [
			{ lname: "鈴木",
			  fname: "太郎",
			  zip: "100-0000",
			  address: "東京都千代田区",
			  phone: "090-9999-9999",
			  st01: false,
			  st02: false,
			  st03: false,
			  st04: false,
			  st05: false
			},
			{ lname: "鈴木",
			  fname: "二郎",
			  zip: "100-0001",
			  address: "東京都千代田区千代田",
			  phone: "090-9999-9999",
			  st01: false,
			  st02: false,
			  st03: false,
			  st04: false,
			  st05: false
			}
		]
	}
];

var GuestModel = function(guest){
	var self = this;
	
	//personal
	self.lname = ko.observable(guest.lname);
	self.fname = ko.observable(guest.fname);
	self.zip = ko.observable(guest.zip);
	self.address = ko.observable(guest.address);
	self.phone = ko.observable(guest.phone);
	self.zipSearch = function(_guest){
		//郵便番号検索
		$.ajax({
			type: "GET",
			url: "http://zipcloud.ibsnet.co.jp/api/search?zipcode=" + _guest.zip(),
			dataType: 'jsonp',
			success: function(res){
				console.log(res);
				if(res.results){
					var addr = res.results[0].address1 + res.results[0].address2 + res.results[0].address3;
					self.address(addr);
				}
			}
		});
	}

	//status
	self.fullname = ko.computed(function() {
		return self.lname() + ' ' + self.fname();
	});
	self.st01 = ko.observable(guest.st01);
	self.st02 = ko.observable(guest.st02);
	self.st03 = ko.observable(guest.st03);
	self.st04 = ko.observable(guest.st04);
	self.st05 = ko.observable(guest.st05);
}

var GroupModel = function(group){
	var self = this;
	self.g_name = group.g_name,
	self.g_category  = ['新郎','新婦'],
	self.g_category_selected  = ko.observable(group.g_category_selected),
	self.members = ko.observableArray(ko.utils.arrayMap(group.members, function(guest) {
		return new GuestModel(guest)
	}))
	self.addGuest = function() {
		self.members.push(
			new GuestModel({
				lname: "",
				fname: "",
				zip: "",
				address: "",
				phone: "",
				st01: false,
				st02: false,
				st03: false,
				st04: false,
				st05: false,
			})
		)
	};
	self.removeGuest = function(guest) {
		self.members.remove(guest);
	};
}

var GroupViewModel = function(groups){
	var self = this;
	self.groups = ko.observableArray(ko.utils.arrayMap(groups, function(group) {
		return new GroupModel(group)
	}));
	self.cnt_groom = ko.observable(0);
	self.cnt_bride = ko.observable(0);
	self.percent_groom = ko.observable("50%");
	self.percent_bride = ko.observable("50%");
	self.total = ko.computed(function() {
		var cnt_guest = 0;
		var cnt_groom = 0;
		var cnt_bride = 0;
		ko.utils.arrayForEach(self.groups(), function(group){
			if(group.g_category_selected() == '新郎') cnt_groom += group.members().length;
			if(group.g_category_selected() == '新婦') cnt_bride += group.members().length;
			cnt_guest += group.members().length;
		});
		//人数更新
		self.cnt_groom(cnt_groom);
		self.cnt_bride(cnt_bride);
		//%更新
		if(cnt_guest != 0){
			var percent_groom = Math.round(cnt_groom/cnt_guest * 100);
			var percent_bride = 100 - percent_groom;
			self.percent_groom( percent_groom + "%" );
			self.percent_bride( percent_bride + "%" );
		}else{
			self.percent_groom( "0%" );
			self.percent_bride( "0%" );
		}
		return cnt_guest;
	}, self);
	self.addGroup = function() {
		self.groups.push(
			new GroupModel({
				g_name: "",
				g_category : ['新郎','新婦'],
				g_category_selected : "新郎",
				members: []
			})
		);
		console.log(self.guests);
	};
	self.removeGroup = function(group) {
		self.groups.remove(group);
	};
	self.addGuest = function(group) {
		group.addGuest();
	};
	self.removeGuest = function(guest) {
		$.each(self.groups(), function() { this.removeGuest(guest) })
	};

	self.save = function(group){
		if (window.localStorage) {
			// if(window.confirm("ブラウザに保存します。よろしいですか？\n※共用のPCでは保存しないで下さい。")){
				var data = ko.utils.stringifyJson(ko.toJS(self.groups));
				console.log(data);
				window.localStorage.setItem('guests', data);
			// }
		}
	}
	self.removeAll = function(group){
		if(window.confirm("データを全て削除してよろしいですか？")){
			self.groups.removeAll();
			if (window.localStorage) {
				window.localStorage.removeItem('guests');
			}
		}
	}
	self.exportJson = function(group){
		ko.utils.stringifyJson(ko.toJS(self.groups));
	}
}

var viewModel;
var data = {};
//データ取り出し
if (window.localStorage) {
	var str_data = window.localStorage.getItem('guests');
	var data = ko.utils.parseJson(str_data);
	console.log(data);
}else{
	window.alert("ローカルストレージが使用できないため、データを保存できません。");
}
if(!data) data = initialData;

$(function(){
	//pager有効化
	viewModel = new GroupViewModel(data);
	pager.extendWithPage(viewModel);
	ko.applyBindings(viewModel);
	
	//イベント登録
	pager.start('personal');
});

//ブラウザ閉じる対策
$(window).on("beforeunload",function(e){
	return "ブラウザにデータを保存してページを移動しますか？\n※共用のPCでは保存しないで下さい。";
});

//electron menuからの呼び出し
var saveData = function(){
	//保存
	viewModel.save();
}
var deleteData = function(){
	//保存済みデータを削除
	viewModel.removeAll();
}
