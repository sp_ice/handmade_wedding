# README #


### About ###

vue.js製の結婚式のゲスト管理ツールです。
Electronで配布します。

### パッケージ手順 ###

```
# npm i electron-packager -g #グローバルにインストール
# cd /path_to_repo
# electron-packager . WeddingGuests --platform=darwin,win32 --arch=x64 --version=0.36.1
```

### アプリのダウンロードはこちら ###
#### Win ####

https://bitbucket.org/sp_ice/handmade_wedding/downloads/WeddingGuests-win32-x64.zip

#### Mac ####

https://bitbucket.org/sp_ice/handmade_wedding/downloads/WeddingGuests-darwin-x64.zip